package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

	/**
	 * Builds a pyramid with sorted values (with minumum value at the top line
	 * and maximum at the bottom, from left to right). All vacant positions in
	 * the array are zeros.
	 *
	 * @param inputNumbers
	 *            to be used in the pyramid
	 * @return 2d array with pyramid inside
	 * @throws {@link
	 *             CannotBuildPyramidException} if the pyramid cannot be build
	 *             with given input
	 */
	public int[][] buildPyramid(List<Integer> inputNumbers) {
		int linesCnt = getLinesCnt(inputNumbers.size());
		checkNumbers(inputNumbers);
		checkNumbersUnique(inputNumbers);

		return fillArray(inputNumbers, linesCnt);
	}

	/**
	 * Pyramid filling
	 */
	private int[][] fillArray(List<Integer> inputNumbers, int linesCnt) {
		int[][] rez = new int[linesCnt][2 * linesCnt - 1];

		int position = 0;
		for (int m = 0; m < linesCnt; m++)
			for (int n = 0; n < m + 1; n++) {
				rez[m][linesCnt - 1 - m + 2 * n] = inputNumbers.get(position++);
			}

		return rez;
	}

	/**
	 * Checking ability of sorting and sorting
	 */
	private boolean checkNumbers(List<Integer> inputNumbers) {
		try {
			// Collections.sort(inputNumbers);
			int tmpValue;
			for (int m = 0; m < inputNumbers.size(); m++)
				for (int n = m + 1; n < inputNumbers.size(); n++) {
					if (inputNumbers.get(m) > inputNumbers.get(n)) {
						tmpValue = inputNumbers.get(m);
						inputNumbers.set(m, inputNumbers.get(n));
						inputNumbers.set(n, tmpValue);
					}
				}
		} catch (Exception e) {
			throw new CannotBuildPyramidException();
		}

		return true;
	}

	/**
	 * Checking duplicate numbers
	 */
	private boolean checkNumbersUnique(List<Integer> inputNumbers) {
		for (int i = 1; i < inputNumbers.size(); i++)
			if (inputNumbers.get(i - 1) - inputNumbers.get(i) == 0)
				throw new CannotBuildPyramidException();

		return true;
	}

	/**
	 * Calculating lines count for value of numbers count
	 */
	private int getLinesCnt(int numbersCnt) {
		float N = (float) ((Math.sqrt(1 + 8 * numbersCnt) - 1) / 2f);
		if ((N == 0) || (N % 1 != 0))
			throw new CannotBuildPyramidException();

		return (int) N;
	}
}
