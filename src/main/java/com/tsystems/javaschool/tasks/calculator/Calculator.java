package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

	/**
	 * Evaluate statement represented as string.
	 *
	 * @param statement
	 *            mathematical statement containing digits, '.' (dot) as decimal
	 *            mark, parentheses, operations signs '+', '-', '*', '/'<br>
	 *            Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return string value containing result of evaluation or null if statement
	 *         is invalid
	 */
	public String evaluate(String statement) {
		if ((statement == null) || (statement.isEmpty()))
			return null;
		while (statement.contains(" "))
			statement = statement.replace(" ", "");
		if (statement.contains(".."))
			return null;

		OPZCalc opz = new OPZCalc();
		if (opz.EqOPZCreate(statement) == null)
			return null;

		return formatRezult(opz.EqOPZCalculate());
	}

	private String formatRezult(float value) {
		NumberFormat nf = new DecimalFormat("#.####");
		String rez = String.valueOf(nf.format(value));
		if (Float.isInfinite(value) || Float.isNaN(value))
			rez = null;
		return rez;
	}

	/**
	 * Reverse Polish notation class
	 */
	class OPZCalc {
		boolean doubleOperator = false;
		boolean unknownChar = false;
		String parsedNumber = "";
		List<String> OPZ = new ArrayList<>();
		List<String> OPZStack = new ArrayList<>();

		/**
		 * Creator of the string with the reverse Polish notation
		 */
		public String EqOPZCreate(String statement) {
			for (int i = 0; i < statement.length(); i++) {
				char c = statement.charAt(i);
				if (doubleOperator && checkOper(c))
					return null;
				if (Character.isDigit(c) || (c == '.')) {
					parsedNumber += c;
					doubleOperator = false;
				} else {
					EqOPZParser(c);
					if (unknownChar)
						return null;
				}
				doubleOperator = checkOper(c);
			}

			if (!parsedNumber.isEmpty())
				OPZ.add(parsedNumber);

			while (!OPZStack.isEmpty()) {
				OPZ.add(OPZStack.get(OPZStack.size() - 1));
				OPZStack.remove(OPZStack.size() - 1);
			}

			return OPZ.toString();
		}

		/**
		 * Analyzing every char of statement string
		 */
		public float EqOPZCalculate() {
			String c = null;
			float val;
			List<Float> vf = new ArrayList<>();
			for (int i = 0; i < OPZ.size(); i++) {
				c = OPZ.get(i);

				switch (c) {
				case "+":
					val = vf.get(vf.size() - 2) + vf.get(vf.size() - 1);
					EqOPZCalculateTrim(vf, val);
					break;
				case "-":
					val = vf.get(vf.size() - 2) - vf.get(vf.size() - 1);
					EqOPZCalculateTrim(vf, val);
					break;
				case "*":
					val = vf.get(vf.size() - 2) * vf.get(vf.size() - 1);
					EqOPZCalculateTrim(vf, val);
					break;
				case "/":
					val = vf.get(vf.size() - 2) / vf.get(vf.size() - 1);
					EqOPZCalculateTrim(vf, val);
					break;
				default:
					try {
						// Float.parseFloat(c);
						vf.add(Float.valueOf(c));
					} catch (Exception e) {
						return Float.NaN;
					}
					break;
				}
			}

			if (vf.size() > 1)
				return Float.NaN;
			return vf.get(0);
		}

		private void EqOPZCalculateTrim(List<Float> vf, float val) {
			vf.remove(vf.size() - 1);
			vf.remove(vf.size() - 1);
			vf.add(val);
		}

		/**
		 * Filling stack and data vectors
		 */
		private void EqOPZParser(char c) {
			String sc = String.valueOf(c);

			switch (sc) {
			case "+":
			case "-":
			case "*":
			case "/":
				if (!parsedNumber.isEmpty())
					OPZ.add(parsedNumber);
				while (!OPZStack.isEmpty() && compRang(OPZStack.get(OPZStack.size() - 1), sc)) {
					OPZ.add(OPZStack.get(OPZStack.size() - 1));
					OPZStack.remove(OPZStack.size() - 1);
				}
				OPZStack.add(sc);
				break;
			case "(":
				OPZStack.add(sc);
				break;
			case ")":
				if (!parsedNumber.isEmpty())
					OPZ.add(parsedNumber);
				while (!OPZStack.isEmpty() && !OPZStack.get(OPZStack.size() - 1).equals("(")) {
					OPZ.add(OPZStack.get(OPZStack.size() - 1));
					OPZStack.remove(OPZStack.size() - 1);
				}
				if (!OPZStack.isEmpty())
					OPZStack.remove(OPZStack.size() - 1);
				break;
			default:
				unknownChar = true;
				break;
			}
			parsedNumber = "";
		}

		private boolean checkOper(char c) {
			String sc = String.valueOf(c);
			if (("+-*/".contains(sc)))
				return true;
			return false;
		}

		private boolean compRang(String h, String l) {
			if (getRang(h) >= getRang(l))
				return true;
			return false;
		}

		/**
		 * List of operators and their priority
		 */
		private int getRang(String c) {
			switch (c) {
			case "+":
			case "-":
				return 2;
			case "*":
			case "/":
				return 3;
			case "(":
			case ")":
				return 1;
			}
			return 0;
		}
	}
}
